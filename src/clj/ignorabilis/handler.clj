(ns ignorabilis.handler
  (:require [compojure.core :refer [GET defroutes]]
            [compojure.route :refer [not-found resources]]
            [ring.middleware.defaults :refer [site-defaults wrap-defaults]]
            [hiccup.core :refer [html]]
            [hiccup.page :as p]
            [prone.middleware :refer [wrap-exceptions]]
            [ring.middleware.reload :refer [wrap-reload]]
            [environ.core :refer [env]]
            [clojure.string :as string]))

(def is-dev (env :is-dev))

(defn inject-devmode-base-paths []
  (when is-dev
    (let [base-paths ["/js/out/goog/base.js"]]
      (apply p/include-js base-paths))))

(defn inject-devmode-ns []
  (when is-dev
    [:script {:type "text/javascript"}
     (str "goog.require('ignorabilis.dev')")]))

(def load-screen
  [:div
   (p/include-css "css/load-screen.css")
   [:div.spinner
    [:div.double-bounce1]
    [:div.double-bounce2]]
   [:h3 "Loading..."]
   [:p "please wait, "
    [:b "it will be worth it!"]]])
(env :is-dev)

(defn get-min-version [css]
  (let [parts (string/split css #"\.")
        min-parts (conj (vec (drop-last parts))
                        "min"
                        (last parts))
        min (string/join "." min-parts)]
    min))

(defn include-env-css [is-dev & css]
  "Include minified version of a css file in production;
  otherwise include the full version."
  (let [css (if is-dev
              css
              (map get-min-version css))]
    (apply p/include-css css)))

(def home-page
  (html
   [:html
    [:head
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport"
             :content "width=device-width, initial-scale=1"}]
     (include-env-css (env :is-dev) "css/app.css")]
    [:body
     [:div#app
      load-screen]
     (inject-devmode-base-paths)
     (p/include-js "js/app.js")
     (inject-devmode-ns)]]))

(defroutes routes
  (GET "/" [] home-page)
  (resources "/")
  (not-found "Not Found"))

(def app
  (let [handler (wrap-defaults #'routes site-defaults)]
    (if (env :is-dev) (-> handler wrap-exceptions wrap-reload) handler)))