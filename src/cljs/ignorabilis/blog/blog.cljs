(ns ignorabilis.blog.blog
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]))

(defn blog-page []
  [:div [:h2 "Some blogging here"]
   [:div [:a {:href "#/"} "go to the home page"]]])
