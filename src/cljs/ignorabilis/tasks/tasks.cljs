(ns ignorabilis.tasks.tasks
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [ignorabilis.tasks.state :as state]
              [ignorabilis.common.utils :as c]))

(defn get-task-status-css [status]
  (str "task-" status))

(defn toggle-task-status [task-status]
  (let [tasks-filter (:tasks-filter @state/state)]
   (if (c/has-value? tasks-filter task-status)
     (swap! state/state assoc :tasks-filter (vec (remove #{task-status} tasks-filter)))
     (swap! state/state assoc :tasks-filter (vec (conj tasks-filter task-status))))))

(defn tasks-filter []
  (vec
   (concat
    [:div
     (for [task-status (:task-statuses @state/state)]
       [:button {:on-click (partial toggle-task-status task-status)} (name task-status)])])))

(defn tasks-list[]
  (vec
   (concat [:ol]
           (let [tasks-filter (:tasks-filter @state/state)
                 all-tasks (:tasks @state/state)
                 tasks (if (seq tasks-filter)
                         (filter #(c/has-value? tasks-filter (:status %)) all-tasks)
                         all-tasks)]
             (for [task tasks
                   :let [{status :status text :text} task
                         status (name status)]]
               [:li
                [:span {:class (get-task-status-css status)} (str "[" status "] ")]  (:text task)])))))

(defn tasks-page []
  [:div
   [:h3 "TO DO list:"]
   (tasks-filter)
   (tasks-list)
   [:a {:href "#/"} "go to the home page"]])
