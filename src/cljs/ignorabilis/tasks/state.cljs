(ns ignorabilis.tasks.state
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]))

(defonce state (atom {:task-statuses [:not-started :in-progress :done :rejected]
                      :tasks []
                      :tasks-filter []}))

(def tasks [{:status :done :text "Create a Reagent project."}
            {:status :done :text "Initialize dependencies."}
            {:status :done :text "Create a few basic pages."}
            {:status :done :text "Read this: "}
            {:status :rejected :text "Add pre-push hook for building uberjar."}
            {:status :done :text "Create and configure Dockerfile to build uberjar and run it on build."}
            {:status :done :text "Fix accounts related to ignorabilis development."}
            {:status :done :text "Configure AWS ECS deployment."}
            {:status :done :text "Configure a Docker autobuild."}
            {:status :done :text "Upload to AWS and start task."}
            {:status :in-progress :text "Figure out a way for AWS ECS to update containers automatically."}
            {:status :in-progress :text "Add DNS and map ignorabilis.com to current ip."}
            {:status :done :text "Change the initial HTML to display a loading message."}
            {:status :done :text "Create hash map for this tasks list – [{:task “” :status “”}]."}
            {:status :done :text "Add meta page for the tasks and list them there (hard coded values)."}
            {:status :done :text "Make the changes of the UI reactive."}
            {:status :done :text "Make filters for the different statuses."}
            {:status :not-started :text "Create hash map for the skills."}
            {:status :not-started :text "Add several hardcoded skills."}
            {:status :not-started :text "Add basic design based on the skills hash map."}
            {:status :not-started :text "Add search box."}
            {:status :not-started :text "Add sente for the client-server WebSockets communication."}
            {:status :not-started :text "Define a robust way to communicate using the WebSockets channel."}
            {:status :not-started :text "Add server data storage and move the skills + meta tasks there."}
            {:status :not-started :text "Add global design."}])

(swap! state assoc :tasks tasks)
