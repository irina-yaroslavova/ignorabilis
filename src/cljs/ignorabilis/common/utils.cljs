(ns ignorabilis.common.utils)

(defn has-value? [coll value]
  (some #{value} coll))
