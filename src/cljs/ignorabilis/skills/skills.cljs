(ns ignorabilis.skills.skills
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]))

(defn skills-page []
  [:div [:h2 "Here I will add a list with all my skills and a search box."]
   [:h3 "(Changed from my phone. Just to demonstrate coolness.)"]
   [:div [:a {:href "#/"} "go to the home page"]]])
