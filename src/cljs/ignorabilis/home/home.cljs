(ns ignorabilis.home.home
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]))

(defn home-page []
  [:div [:h2 "Welcome to ignorabilis!"]
   [:div
    [:a {:href "#/skills"} "skills"]
    [:br]
    [:a {:href "#/blog"} "blog"]
    [:br]
    [:a {:href "#/tasks"} "tasks"]]])
