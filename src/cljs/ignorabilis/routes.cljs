(ns ignorabilis.routes
    (:require [reagent.core :as reagent :refer [atom]]
              [reagent.session :as session]
              [secretary.core :as secretary :include-macros true]
              [goog.events :as events]
              [goog.history.EventType :as EventType]

              ;;; views ns
              [ignorabilis.home.home :as home]
              [ignorabilis.skills.skills :as skills]
              [ignorabilis.blog.blog :as blog]
              [ignorabilis.tasks.tasks :as tasks])
    (:import goog.History))

(defn register-client-routes []
  (secretary/set-config! :prefix "#")

  (secretary/defroute "/" []
    (session/put! :current-page #'home/home-page))

  (secretary/defroute "/skills" []
    (session/put! :current-page #'skills/skills-page))

  (secretary/defroute "/blog" []
    (session/put! :current-page #'blog/blog-page))

  (secretary/defroute "/tasks" []
    (session/put! :current-page #'tasks/tasks-page)))
