(ns app
  (:require [garden.def :refer [defstylesheet defstyles]]
            [garden.units :refer [px percent em]]
            [garden.color :as color :refer [hsl rgb]]
            [garden.stylesheet :refer [at-media]]))

(def primary-fonts ["'Helvetica Neue'" "Verdana" "Helvetica" "Arial" "sans-serif"])
(def primary-font-color (rgb 33 33 33))
(def header-color (rgb 0 0 0))
(def anchor-color (rgb 0 153 255))

(def task-not-started-color (rgb 99 11 11))
(def task-in-progress-color (rgb 230 200 11))
(def task-done-color (rgb 11 190 55))
(def task-rejected-color (rgb 150 150 150))

(defn get-task-status-css [status]
  (str ".task-" status))

(def layout
  [:body
   {:font-family primary-fonts
    :max-width (px 800)
    :margin "0 auto"
    :padding-top (px 72)
    :-webkit-font-smoothing "antialiased"
    :font-size (em 1.125)
    :color primary-font-color
    :line-height (em 1.5)}

   [:h1 :h2 :h3
    {:color header-color}]

   [:h1
    {:font-size (em 2.5)}]

   [:h2
    {:font-size (em 2)}]

   [:h3
    {:font-size (em 1.5)}]

   [:a
    {:text-decoration "none"
     :color anchor-color}
    [:&:hover
     {:text-decoration "underline"}]]])

(def tasks
  [:body
   [(get-task-status-css "not-started")
    {:color task-not-started-color}]
   [(get-task-status-css "in-progress")
    {:color task-in-progress-color}]
   [(get-task-status-css "done")
    {:color task-done-color}]
   [(get-task-status-css "rejected")
    {:color task-rejected-color}]])

(defstyles app
  layout
  tasks)
