#
# Dockerfile for my personal website
#

FROM clojure
MAINTAINER Ivan Stefanov <ivan.yaroslavov@svarogg.com>
ENV REFRESHED_AT 2015-07-17

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY project.clj /usr/src/app/
RUN lein deps
COPY . /usr/src/app
RUN lein garden once
RUN lein uberjar
CMD ["java", "-jar", "target/ignorabilis.jar"]

ENV PORT 4000

EXPOSE 4000
