(defproject ignorabilis "0.1.0-SNAPSHOT"
  :description "Discovering the known unknowns - a place to track the quantum conversion of knowledge."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :source-paths ["src/clj" "src/cljs" "src/cljc" "src/css"]

  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "1.7.145"]
                 [ring-server "0.4.0"]
                 [cljsjs/react "0.13.3-0"]
                 [reagent "0.5.0"]
                 [reagent-forms "0.5.1"]
                 [reagent-utils "0.1.5"]
                 [ring "1.3.2"]
                 [ring/ring-defaults "0.1.5"]
                 [prone "0.8.2"]
                 [compojure "1.3.4"]
                 [hiccup "1.0.5"]
                 [environ "1.0.0"]
                 [secretary "1.2.3"]
                 [garden "1.2.5"]
                 [figwheel-sidecar "0.4.0"]]

  :plugins [[lein-pdo "0.1.1"]
            [lein-environ "1.0.0"]
            [lein-asset-minifier "0.2.2"]]

  :ring {:handler ignorabilis.handler/app}
  :min-lein-version "2.5.0"
  :uberjar-name "ignorabilis.jar"
  :main ignorabilis.server

  :clean-targets ^{:protect false} [:target-path
                                    [:cljsbuild :builds :app :compiler :output-dir]
                                    [:cljsbuild :builds :app :compiler :output-to]]

  :minify-assets
  {:assets
    {"resources/public/css/app.min.css" "resources/public/css/app.css"}}

  :cljsbuild {:builds {:app {:source-paths ["src/cljs"]
                             :compiler {:output-to     "resources/public/js/app.js"
                                        :output-dir    "resources/public/js/out"
                                        :asset-path   "js/out"
                                        :optimizations :none
                                        :pretty-print  true}}}}

  :profiles {:dev  {:repl-options {:init-ns          ignorabilis.repl
                                      :nrepl-middleware []}

                       :dependencies [[ring-mock "0.1.5"]
                                      [ring/ring-devel "1.3.2"]
                                      [leiningen-core "2.5.1"]
                                      [lein-figwheel "0.3.5"]
                                      [org.clojure/tools.nrepl "0.2.10"]
                                      [pjstadig/humane-test-output "0.7.0"]]

                       :source-paths ["env/dev/clj"]
                       :plugins      [[lein-garden "0.2.5"]
                                      [lein-cljsbuild "1.0.6"]]

                       :injections   [(require 'pjstadig.humane-test-output)
                                      (pjstadig.humane-test-output/activate!)]

                       :figwheel     {:http-server-root "public"
                                      :server-port      3449
                                      :nrepl-port       7002
                                      :css-dirs         ["resources/public/css"]
                                      :ring-handler     ignorabilis.handler/app}

                       :env          {:is-dev true}

                       :cljsbuild    {:builds {:app {:source-paths ["env/dev/cljs"]
                                                     :compiler     {:output-dir "resources/public/js/out"
                                                                    :source-map "resources/public/js/app.js.map"}}}}

                       :garden       {:builds [{:id           "app"
                                                :source-paths ["src/css"]
                                                :stylesheet   app/app
                                                :compiler     {:output-to     "resources/public/css/app.css"
                                                               :pretty-print? true}}]}

                       :aliases      {"interactive" ["pdo" "cljsbuild" "auto," "garden" "auto"]}}

             :uberjar {:hooks       [leiningen.cljsbuild minify-assets.plugin/hooks]
                       :env         {:is-prod true}
                       :aot         :all
                       :omit-source true
                       :cljsbuild   {:jar    true
                                     :builds {:app
                                              {:source-paths ["env/prod/cljs"]
                                               :compiler
                                                             {:optimizations :advanced
                                                              :pretty-print  false}}}}}})
